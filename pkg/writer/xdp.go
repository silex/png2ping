package writer

import (
	"fmt"
	"math"
	"net"

	"github.com/asavie/xdp"

	"git.slxh.eu/silex/png2ping/pkg/packet"
)

// XDP is a writer to an XDP packet connection.
type XDP struct {
	conn  *xdp.Socket
	iface *net.Interface
}

// NewXDP creates a Writer for the given source address and buffer size.
// The buffer size is used for determining the batch size of writes.
func NewXDP(iface *net.Interface, queue int) (Writer, error) {
	conn, err := xdp.NewSocket(iface.Index, queue, nil)
	if err != nil {
		return nil, fmt.Errorf("create socket: %w", err)
	}

	return &XDP{
		conn:  conn,
		iface: iface,
	}, nil
}

// Run writes packets from the given channel to the underlying interface
// until the channel is closed.
func (w *XDP) Run(ch <-chan []packet.Packet) {
	descs := w.conn.GetDescs(math.MaxInt32, false)

	for m := range ch {
		for {
			n := min(len(m), min(len(descs), w.conn.NumFreeTxSlots()))

			for i, pkt := range m[:n] {
				copy(w.conn.GetFrame(descs[i]), pkt)
				descs[i].Len = uint32(len(pkt))
			}

			w.conn.Transmit(descs[:n])

			_, c, err := w.conn.Poll(1)
			if err != nil {
				panic(err)
			}

			m = m[min(c, n):]
			if len(m) == 0 {
				break
			}
		}
	}
}

// Write writes a batch of messages to the underlying interface.
func (w *XDP) Write(m []packet.Packet) {
	//for _, packet := range m {
	//
	//}
}

// Close closes all open resources.
func (w *XDP) Close() {
	w.conn.Close()
}
