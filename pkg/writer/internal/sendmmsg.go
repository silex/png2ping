package internal

import (
	"fmt"
	"syscall"
	"unsafe"

	"golang.org/x/sys/unix"

	"git.slxh.eu/silex/png2ping/pkg/packet"
)

type MessageHeader struct {
	msghdr unix.Msghdr
	msglen uint32
	_      [4]byte
}

type SockaddrLinklayer struct {
	Protocol uint16
	Ifindex  int
	Hatype   uint16
	Pkttype  uint8
	Halen    uint8
	Addr     [8]byte
	raw      syscall.RawSockaddrLinklayer
}

func (sa *SockaddrLinklayer) sockaddr() (unsafe.Pointer, uint32) {
	if sa.Ifindex < 0 || sa.Ifindex > 0x7fffffff {
		panic("invalid interface")
	}

	sa.raw.Family = syscall.AF_PACKET
	sa.raw.Protocol = sa.Protocol
	sa.raw.Ifindex = int32(sa.Ifindex)
	sa.raw.Hatype = sa.Hatype
	sa.raw.Pkttype = sa.Pkttype
	sa.raw.Halen = sa.Halen
	sa.raw.Addr = sa.Addr

	return unsafe.Pointer(&sa.raw), syscall.SizeofSockaddrLinklayer
}

func BuildHeaders(dst []MessageHeader, packets []packet.Packet, sa *SockaddrLinklayer) {
	saPtr, saLen := sa.sockaddr()

	for i, pkt := range packets {
		if dst[i].msghdr.Iov == nil {
			dst[i].msghdr.Iov = &unix.Iovec{}
		}

		dst[i].msghdr.Name = (*byte)(saPtr)
		dst[i].msghdr.Namelen = saLen
		dst[i].msghdr.Iov.Base = &pkt[0]
		dst[i].msghdr.Iov.Len = uint64(len(pkt))
		dst[i].msghdr.Iovlen = uint64(1)
		dst[i].msghdr.Control = nil
		dst[i].msghdr.Controllen = 0
		dst[i].msghdr.Flags = 0
	}
}

func SendMessages(fd int, headers []MessageHeader, flags int) (int, error) {
	n, _, errno := syscall.Syscall6(
		unix.SYS_SENDMMSG,
		uintptr(fd),
		uintptr(unsafe.Pointer(&headers[0])),
		uintptr(len(headers)),
		uintptr(flags),
		0, 0,
	)

	if errno != 0 {
		return 0, fmt.Errorf("sendmmsg: %w", errno)
	}

	return int(n), nil
}
