package writer

import (
	"git.slxh.eu/silex/png2ping/pkg/packet"
)

// DevNull represents a throw-away writer.
type DevNull struct{}

// NewDevNull returns an initialised throw-away writer.
func NewDevNull() Writer {
	return new(DevNull)
}

// Run writes packets from the given channel to the underlying interface
// until the channel is closed.
func (w *DevNull) Run(ch <-chan []packet.Packet) {
	for m := range ch {
		w.Write(m)
	}
}

// Write writes a batch of messages to the underlying interface.
func (w *DevNull) Write(m []packet.Packet) {
	// do nothing
}

// Close closes all open resources.
func (w *DevNull) Close() {
	// do nothing
}
