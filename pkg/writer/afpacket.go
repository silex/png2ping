//go:build !windows

package writer

import (
	"fmt"
	"log"
	"net"
	"syscall"

	"git.slxh.eu/silex/png2ping/pkg/packet"
	"git.slxh.eu/silex/png2ping/pkg/writer/internal"
)

// AFPacket is a writer to an AFPacket packet connection.
type AFPacket struct {
	conn  int
	iface *net.Interface
}

// NewAFPacket creates a Writer for the given source address and buffer size.
// The buffer size is used for determining the batch size of writes.
func NewAFPacket(iface *net.Interface) (Writer, error) {
	conn, err := syscall.Socket(syscall.AF_PACKET, syscall.SOCK_RAW, int(htons(syscall.ETH_P_ALL)))
	if err != nil {
		return nil, fmt.Errorf("create socket: %w", err)
	}

	return &AFPacket{
		conn:  conn,
		iface: iface,
	}, nil
}

// Run writes packets from the given channel to the underlying interface
// until the channel is closed.
func (w *AFPacket) Run(ch <-chan []packet.Packet) {
	headers := make([]internal.MessageHeader, 1024)
	addr := &internal.SockaddrLinklayer{
		Protocol: htons(syscall.ETH_P_ALL),
		Ifindex:  w.iface.Index,
	}

	for m := range ch {
		for {
			n := min(len(m), len(headers))

			internal.BuildHeaders(headers, m[:n], addr)

			c, err := internal.SendMessages(w.conn, headers[:n], 0)
			if err != nil {
				log.Printf("Error sending messages: %s", err)
			}

			if c == len(m) {
				break
			}

			m = m[c:]
		}
	}
}

// Write writes a batch of messages to the underlying interface.
func (w *AFPacket) Write(m []packet.Packet) {
	headers := make([]internal.MessageHeader, 1024)
	addr := &internal.SockaddrLinklayer{
		Protocol: htons(syscall.ETH_P_ALL),
		Ifindex:  w.iface.Index,
	}

	for {
		n := min(len(m), len(headers))

		internal.BuildHeaders(headers, m[:n], addr)

		c, err := internal.SendMessages(w.conn, headers[:n], 0)
		if err != nil {
			log.Printf("Error sending messages: %s", err)
		}

		if c == len(m) {
			break
		}

		m = m[c:]
	}
}

// Close closes all open resources.
func (w *AFPacket) Close() {
	syscall.Close(w.conn)
}

// htons converts a uint16 from host- to network byte order.
func htons(i uint16) uint16 {
	return (i<<8)&0xff00 | i>>8
}
