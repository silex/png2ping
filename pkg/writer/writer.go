package writer

import (
	"git.slxh.eu/silex/png2ping/pkg/packet"
)

// Writer represents a packet writer.
type Writer interface {
	// Run writes packets from the given channel to the underlying interface
	// until the channel is closed.
	Run(ch <-chan []packet.Packet)

	// Write writes a batch of messages to the underlying interface.
	Write(m []packet.Packet)

	// Close closes all open resources.
	Close()
}
