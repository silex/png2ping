package address

import (
	"encoding/binary"
	"fmt"
	"image/color"
	"net/netip"
)

const colorOffset = 12

var _ Generator = (*RGBA)(nil)

// RGBA represents a generic RGBA generator.
// The pixel format for this is <prefix>:<x>:<y>:<c><c>:<c><c>,
// where c can be the R, G, B or A value.
type RGBA struct {
	base netip.Addr
	mode [4]int
	opts GeneratorOpts
}

// NewRGBA returns an RGBA generator for the given prefix and mode.
// The mode is decoded into the color order, eg: "rgba".
func NewRGBA(prefix string, opts GeneratorOpts) (*RGBA, error) {
	ip, err := netip.ParseAddr(prefix)
	if err != nil {
		return nil, fmt.Errorf("parse prefix: %w", err)
	}

	m, err := getMode(opts.ColorMode)
	if err != nil {
		return nil, fmt.Errorf("invalid mode %q: %s", prefix, err)
	}

	return &RGBA{base: ip, mode: m, opts: opts}, nil
}

// Convert converts the x/y/color pixel value into an IPv6 address.
func (n *RGBA) Convert(x, y int, c color.Color) netip.Addr {
	x += n.opts.Offset.X
	y += n.opts.Offset.Y
	r, g, b, a := c.RGBA()

	// Ignore invisible pixels
	if x < 0 || x >= n.opts.CanvasSize.X || y < 0 || y >= n.opts.CanvasSize.Y || a == 0 {
		return netip.Addr{}
	}

	ip := n.base.As16()

	binary.BigEndian.PutUint16(ip[8:], uint16(x))
	binary.BigEndian.PutUint16(ip[10:], uint16(y))

	ip[n.mode[0]] = byte(r >> 8)
	ip[n.mode[1]] = byte(g >> 8)
	ip[n.mode[2]] = byte(b >> 8)
	ip[n.mode[3]] = byte(a >> 8)

	return netip.AddrFrom16(ip)
}

// getMode decodes the mode string into four integers representing the pixel order.
func getMode(mode string) ([4]int, error) {
	var m [4]int
	if len(mode) != 4 {
		return m, fmt.Errorf("invalid length")
	}

	for i, c := range mode {
		switch c {
		case 'r':
			m[0] = i
		case 'g':
			m[1] = i
		case 'b':
			m[2] = i
		case 'a':
			m[3] = i
		default:
			return m, fmt.Errorf("invalid character: %q", c)
		}
	}

	for i := range m {
		m[i] += colorOffset
	}

	return m, nil
}
