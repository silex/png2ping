package address

import (
	"fmt"
	"image/color"
	"net/netip"
	"strings"
)

const legacyFormat = "%s:%04d:%04d:%04x:%04x:%04x" // prefix, x, y, r, g, b

// Legacy implements the original IPv6 Christmas tree LED screen format.
// The pixel format for this is: <prefix>:<x>:<y>:<r>:<g>:<b>,
// with x/y in decimal and r/g/b in hexadecimal.
type Legacy struct {
	prefix string
	opts   GeneratorOpts
}

// NewLegacy returns a configured Legacy Generator.
func NewLegacy(prefix string, options GeneratorOpts) Generator {
	return &Legacy{
		prefix: strings.TrimSuffix(prefix, "::"),
		opts:   options,
	}
}

// Convert converts the x/y/color pixel value into an IPv6 address.
func (l *Legacy) Convert(x, y int, c color.Color) netip.Addr {
	x += l.opts.Offset.X
	y += l.opts.Offset.Y
	r, g, b, a := c.RGBA()

	// Ignore invisible pixels
	if x < 0 || x >= l.opts.CanvasSize.X || y < 0 || y >= l.opts.CanvasSize.Y || a == 0 {
		return netip.Addr{}
	}

	s := fmt.Sprintf(legacyFormat, l.prefix, x, y, b>>8, g>>8, r>>8)

	return netip.MustParseAddr(s)
}
