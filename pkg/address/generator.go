package address

import (
	"image"
	"image/color"
	"net/netip"
)

// Generator represents a pixel to IPv6 address generator.
type Generator interface {
	// Convert converts the x/y/color pixel value into an IPv6 address.
	Convert(x, y int, c color.Color) netip.Addr
}

// GeneratorOpts contains the options for initializing a generator.
type GeneratorOpts struct {
	// Color mode to use.
	ColorMode string

	// Pixel offsets.
	Offset image.Point

	// Canvas size
	CanvasSize image.Point
}
