package address

import (
	"image/color"
	"testing"
)

const prefix = "2001:db8::"

func benchmarkConvert(b *testing.B, g Generator) {
	p := color.RGBA{R: 0xff, G: 0xd1, B: 0x00, A: 0xff}
	for i := 0; i < b.N; i++ {
		g.Convert(15, 812, p)
	}
}

func BenchmarkLegacy_Convert(b *testing.B) {
	benchmarkConvert(b, NewLegacy(prefix, 1920, 1080))
}

func BenchmarkRGBA_Convert(b *testing.B) {
	g, _ := NewRGBA(prefix, "rgba", 1920, 1080)
	benchmarkConvert(b, g)
}

func BenchmarkTree_Convert(b *testing.B) {
	g, _ := NewTree(prefix)
	benchmarkConvert(b, g)
}
