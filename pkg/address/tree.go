package address

import (
	"fmt"
	"image/color"
	"net/netip"
)

var _ Generator = Tree{}

// Tree implements the Generator for the simple IPv6 Christmas tree.
// The pixel format of this is <prefix>:<r>:<g>:<b> (in hex).
// Note that there is no x/y information.
type Tree struct {
	base netip.Addr
}

// NewTree returns a Tree Generator for the given prefix.
func NewTree(prefix string) (Tree, error) {
	ip, err := netip.ParseAddr(prefix)
	if err != nil {
		return Tree{}, fmt.Errorf("parse prefix: %w", err)
	}

	return Tree{base: ip}, nil
}

// Convert converts the x/y/color pixel value into an IPv6 address.
func (t Tree) Convert(_, _ int, c color.Color) netip.Addr {
	r, g, b, _ := c.RGBA()

	ip := t.base.As16()
	ip[11] = byte(r >> 8)
	ip[13] = byte(g >> 8)
	ip[15] = byte(b >> 8)

	return netip.AddrFrom16(ip)
}
