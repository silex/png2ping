package netutil

import (
	"errors"
	"fmt"
	"log/slog"
	"net"

	"github.com/vishvananda/netlink"
)

var (
	ErrNoInterface = errors.New("no such interface")
	ErrNoNextHop   = errors.New("no next hop found")
)

// InterfaceForAddr returns the interface with the given address if it exists.
func InterfaceForAddr(addr net.IP) (*net.Interface, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, fmt.Errorf("list interfaces: %w", err)
	}

	for _, iface := range ifaces {
		addrs, err := iface.Addrs()
		if err != nil {
			continue
		}

		for _, a := range addrs {
			if ipnet, ok := a.(*net.IPNet); ok && ipnet.IP.Equal(addr) {
				return &iface, nil
			}
		}
	}

	return nil, ErrNoInterface
}

// NextHopForAddr returns the next hop for the given address.
func NextHopForAddr(addr net.IP) (net.HardwareAddr, error) {
	routes, err := netlink.RouteGet(addr)
	if err != nil {
		return nil, fmt.Errorf("get route: %w", err)
	}

	for _, route := range routes {
		if route.Gw == nil {
			return nil, nil
		}

		neighs, err := netlink.NeighList(route.LinkIndex, netlink.FAMILY_V6)
		if err != nil {
			continue
		}

		for _, neigh := range neighs {
			slog.Info("Resolving neigh", "neigh", neigh)

			if neigh.IP.Equal(route.Gw) {
				return neigh.HardwareAddr, nil
			}
		}
	}

	return nil, ErrNoNextHop
}
