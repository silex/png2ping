package color

import (
	"image/color"
	"math"

	"git.slxh.eu/silex/png2ping/pkg/address"
	"git.slxh.eu/silex/png2ping/pkg/packet"
	"git.slxh.eu/silex/png2ping/pkg/pixel"
)

// Rainbow is a single pixel rainbow fade generator.
type Rainbow struct {
	*pixel.Stream
	opts      packet.Creator
	generator address.Generator
	x, y      int
	size      float64
}

// NewRainbow returns a generator for a single-pixel rainbow fade effect.
func NewRainbow(x, y, size int, options pixel.GeneratorOptions) pixel.Stream {
	return pixel.Stream{
		Size: size,
		Gen: func(i int) packet.Packet {
			dst := options.Generator.Convert(x, y, getColor(float64(i)/float64(size)))

			return options.NewPacket(dst, i)
		},
	}
}

// getColor returns the color for a time `t` in the rainbow.
// The period of the rainbow fade is 1.
func getColor(t float64) (c color.RGBA) {
	c.R = sin(t, 0/3.)
	c.G = sin(t, 1/3.)
	c.B = sin(t, 2/3.)
	c.A = 0xff
	return
}

// sin calculates the 8-bit sine value for a given timestamp and time offset.
func sin(t, offs float64) uint8 {
	return uint8(math.Round(255 * (math.Sin(2*math.Pi*(t-offs))/2 + 0.5)))
}
