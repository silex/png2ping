package color

import (
	"image/color"

	"git.slxh.eu/silex/png2ping/pkg/packet"
	"git.slxh.eu/silex/png2ping/pkg/pixel"
)

// NewStatic return a Generator for the given pixel color and location.
func NewStatic(x, y int, color color.Color, opts pixel.GeneratorOptions) pixel.Stream {
	return pixel.Stream{
		Size: pixel.BufferSize,
		Gen: func(i int) packet.Packet {
			return opts.NewPacket(opts.Generator.Convert(x, y, color), i)
		},
	}
}
