package image

import (
	"log/slog"
	"sync/atomic"
	"time"

	"golang.org/x/net/context"

	"git.slxh.eu/silex/png2ping/pkg/packet"
	"git.slxh.eu/silex/png2ping/pkg/pixel"
)

var _ pixel.Generator = (*Updater)(nil)

// Updater implements a [pixel.Generator] that updates an image from a given source.
type Updater struct {
	get func() (pixel.Generator, error)
	ptr atomic.Pointer[pixel.Generator]
}

// NewGetter returns a [Updater] that updates the image based on the given function.
// The source is not refreshed when the given interval is zero.
func NewGetter(interval time.Duration, get func() (pixel.Generator, error)) (*Updater, error) {
	w := &Updater{
		get: get,
	}

	if interval > 0 {
		go w.refresh(interval)
	}

	return w, w.update()
}

func (w *Updater) update() error {
	gen, err := w.get()
	if err != nil {
		return err
	}

	w.ptr.Store(&gen)

	return nil
}

func (w *Updater) refresh(interval time.Duration) {
	ticker := time.NewTicker(interval)
	defer ticker.Stop()

	for range ticker.C {
		err := w.update()
		if err != nil {
			slog.Error("Unable to refresh web image: %w", err)
		}
	}
}

func (w *Updater) generator() pixel.Generator {
	g := w.ptr.Load()
	return *g
}

func (w *Updater) Generate(ctx context.Context, n int, ch chan<- []packet.Packet) {
	for range n {
		select {
		case <-ctx.Done():
			return
		default:
			w.generator().Generate(ctx, 1, ch)
		}
	}
}
