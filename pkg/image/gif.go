package image

import (
	"image/gif"
	"io"

	"golang.org/x/net/context"

	"git.slxh.eu/silex/png2ping/pkg/packet"
	"git.slxh.eu/silex/png2ping/pkg/pixel"
)

var _ pixel.Generator = GIF{}

// GIF is a Generator that generates the pixels from a GIF image file.
type GIF struct {
	frames [][]packet.Packet
	repeat int
}

// NewGIF initialises a GIF from the given image file,
// with the given parameters.
func NewGIF(r io.Reader, options pixel.GeneratorOptions) (GIF, error) {
	animation, err := gif.DecodeAll(r)
	if err != nil {
		return GIF{}, err
	}

	generator := GIF{
		frames: make([][]packet.Packet, 0, len(animation.Image)),
		repeat: options.RepeatFrame,
	}

	for _, frame := range animation.Image {
		generator.frames = append(generator.frames, generatePackets(frame, options))
	}

	return generator, nil
}

func (g GIF) Generate(ctx context.Context, n int, ch chan<- []packet.Packet) {
	for range n {
		for _, frame := range g.frames {
			for range g.repeat {
				select {
				case ch <- frame:
				case <-ctx.Done():
					return
				}
			}
		}
	}
}
