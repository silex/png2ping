package image

import (
	"image"
	"image/png"
	"io"

	"git.slxh.eu/silex/png2ping/pkg/pixel"
)

var _ pixel.Generator = Image{}

// Image represents a generic static image.
// It implements [pixel.Generator].
type Image struct {
	pixel.Buffer
}

// NewImageGenerator returns a Generator for the given image and offsets.
// The pixel order can be randomized.
func NewImageGenerator(img image.Image, options pixel.GeneratorOptions) Image {
	return Image{Buffer: generatePackets(img, options)}
}

// NewPNG initialises an Image from a PNG data stream.
func NewPNG(r io.Reader, options pixel.GeneratorOptions) (Image, error) {
	img, err := png.Decode(r)
	if err != nil {
		return Image{}, err
	}

	return NewImageGenerator(img, options), nil
}
