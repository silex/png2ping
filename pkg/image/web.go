package image

import (
	"fmt"
	"net/http"

	"git.slxh.eu/silex/png2ping/pkg/pixel"
)

// FromWeb returns an [Updater] that refreshes from a given URL.
func FromWeb(uri string, options pixel.GeneratorOptions) (*Updater, error) {
	return NewGetter(options.ReloadInterval, func() (pixel.Generator, error) {
		return loadWeb(uri, options)
	})
}

func loadWeb(uri string, options pixel.GeneratorOptions) (pixel.Generator, error) {
	resp, err := http.Get(uri)
	if err != nil {
		return nil, fmt.Errorf("get web image: %w", err)
	}
	defer resp.Body.Close()

	switch resp.Header.Get("Content-type") {
	case "image/png":
		return NewPNG(resp.Body, options)
	case "image/gif":
		return NewGIF(resp.Body, options)
	default:
		return nil, fmt.Errorf("%w: %q", ErrUnsupportedFile, resp.Header.Get("Content-type"))
	}
}
