package image

import (
	"errors"
	"os"
	"path/filepath"

	"git.slxh.eu/silex/png2ping/pkg/pixel"
)

// ErrUnsupportedFile is returned when the given file is not supported.
var ErrUnsupportedFile = errors.New("unsupported file type")

// FromFile returns an [Updater] that refreshes from a given file.
func FromFile(path string, options pixel.GeneratorOptions) (*Updater, error) {
	return NewGetter(options.ReloadInterval, func() (pixel.Generator, error) {
		return loadFile(path, options)
	})
}

func loadFile(path string, options pixel.GeneratorOptions) (pixel.Generator, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	switch filepath.Ext(path) {
	case ".png":
		return NewPNG(file, options)
	case ".gif":
		return NewGIF(file, options)
	default:
		return nil, ErrUnsupportedFile
	}
}
