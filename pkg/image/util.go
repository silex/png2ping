package image

import (
	"image"
	"math/rand/v2"
	"net/netip"

	"git.slxh.eu/silex/png2ping/pkg/packet"
	"git.slxh.eu/silex/png2ping/pkg/pixel"
)

func generatePackets(img image.Image, options pixel.GeneratorOptions) []packet.Packet {
	size := img.Bounds().Max

	ips := make([]netip.Addr, 0, size.X*size.Y)
	for y := 0; y < size.Y; y++ {
		for x := 0; x < size.X; x++ {
			ip := options.Generator.Convert(x, y, img.At(x, y))
			if ip.IsValid() {
				ips = append(ips, ip)
			}
		}
	}

	if options.Randomize {
		rand.Shuffle(len(ips), func(i, j int) {
			ips[i], ips[j] = ips[j], ips[i]
		})
	}

	msgs := make([]packet.Packet, len(ips))
	for i, dst := range ips {
		msgs[i] = options.Creator.NewPacket(dst, i)
	}

	return msgs
}
