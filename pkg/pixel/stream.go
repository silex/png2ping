package pixel

import (
	"context"

	"git.slxh.eu/silex/png2ping/pkg/packet"
)

var _ Generator = Stream{}

// Stream is a [Generator] that generates pixels based on a given function.
// Generated results are buffered in memory.
type Stream struct {
	// Size contains the number of pixels to iterate over.
	Size int

	// Gen contains a function that generates a single packet.
	Gen func(i int) packet.Packet
}

// Generate implements [Generator].
func (b Stream) Generate(ctx context.Context, n int, ch chan<- []packet.Packet) {
	buf := make([]packet.Packet, b.Size)

	for range n {
		for j := range b.batchCount() {
			select {
			case ch <- b.getBatch(buf, j):
			case <-ctx.Done():
				return
			}
		}
	}
}

func (b Stream) batchCount() int {
	return b.Size/BufferSize + 1
}

func (b Stream) getBatch(buf []packet.Packet, i int) []packet.Packet {
	j := i * BufferSize
	n := min(len(buf), j+BufferSize)
	s := buf[j:n]

	for k, p := range s {
		if p == nil {
			s[k] = b.Gen(k + j)
		}
	}

	return s
}
