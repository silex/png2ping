package pixel

import (
	"golang.org/x/net/context"

	"git.slxh.eu/silex/png2ping/pkg/packet"
)

var _ Generator = Buffer{}

// Buffer is a [Generator] that produces the packets from the given buffer.
type Buffer []packet.Packet

// Generate implements [pixel.Generator].
func (b Buffer) Generate(ctx context.Context, n int, ch chan<- []packet.Packet) {
	for range n {
		select {
		case ch <- b:
		case <-ctx.Done():
			return
		}
	}
}
