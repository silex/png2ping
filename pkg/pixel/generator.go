package pixel

import (
	"time"

	"golang.org/x/net/context"

	"git.slxh.eu/silex/png2ping/pkg/address"
	"git.slxh.eu/silex/png2ping/pkg/packet"
)

// BufferSize is the default buffer size for a set of IPv6 messages.
const BufferSize = 1024

// Generator represents an IPv6 message generator.
type Generator interface {
	// Generate pushes the generation result `n` times to the given channel `ch`.
	// The given channel is not closed afterward, and may be used again.
	Generate(ctx context.Context, n int, ch chan<- []packet.Packet)
}

// GeneratorOptions contains the options for initializing a [Generator].
type GeneratorOptions struct {
	packet.Creator

	// Generator to use for generating destination addresses.
	Generator address.Generator

	// Randomize the pixel order.
	Randomize bool

	// Number of times to repeat frames when the file is animated.
	RepeatFrame int

	// Skip transparent pixels
	SkipTransparent bool

	// Interval on which to refresh the image.
	ReloadInterval time.Duration
}
