package packet

import (
	"encoding/binary"
	"fmt"
	"log/slog"
	"net"
	"net/netip"
	"os"

	"git.slxh.eu/silex/png2ping/pkg/netutil"
)

// Packet represents a raw ICMP packet.
// It includes Ethernet, VLAN and PPPoE frames if required.
type Packet []byte

// CreatorOpts contains the options for generating packets.
type CreatorOpts struct {
	SourceAddr string
	DestAddr   string
	PPPoE      struct {
		Interface string
		NextHop   string
		SessionID int
		VLAN      int
	}
}

// Creator contains the options for creating a packet.
type Creator struct {
	PID        uint16
	SourceIP   netip.Addr
	SourceLink *net.Interface
	SourceMAC  net.HardwareAddr
	DestMAC    net.HardwareAddr
	PPPoE      struct {
		Interface *net.Interface
		SourceMAC net.HardwareAddr
		DestMAC   net.HardwareAddr
		SessionID uint16
		VLAN      uint16
	}
}

// NewCreator returns an initialized Creator based on the given options.
func NewCreator(options CreatorOpts) (Creator, error) {
	srcAddr, err := netip.ParseAddr(options.SourceAddr)
	if err != nil {
		return Creator{}, fmt.Errorf("parse source addr: %w", err)
	}

	dstAddr, err := netip.ParseAddr(options.DestAddr)
	if err != nil {
		return Creator{}, fmt.Errorf("parse destination addr: %w", err)
	}

	iface, err := netutil.InterfaceForAddr(srcAddr.AsSlice())
	if err != nil {
		return Creator{}, fmt.Errorf("get interface for src: %w", err)
	}

	dstMAC, err := netutil.NextHopForAddr(dstAddr.AsSlice())
	if err != nil {
		return Creator{}, fmt.Errorf("get next hop for dst: %w", err)
	}

	f := Creator{
		PID:        uint16(os.Getpid() & 0xffff),
		SourceIP:   srcAddr,
		SourceLink: iface,
		SourceMAC:  iface.HardwareAddr,
		DestMAC:    dstMAC,
	}

	if f.SourceMAC == nil {
		slog.Warn("Using all-zero source MAC")
		f.SourceMAC = net.HardwareAddr{0, 0, 0, 0, 0, 0}
	}

	if f.DestMAC == nil {
		slog.Warn("Using all-zero destination MAC")
		f.DestMAC = net.HardwareAddr{0, 0, 0, 0, 0, 0}
	}

	if options.PPPoE.Interface != "" {
		f.PPPoE.Interface, err = net.InterfaceByName(options.PPPoE.Interface)
		if err != nil {
			return Creator{}, fmt.Errorf("lookup PPPoE interface %w", err)
		}

		f.PPPoE.DestMAC, err = net.ParseMAC(options.PPPoE.NextHop)
		if err != nil {
			return Creator{}, fmt.Errorf("parse PPPoE next op %w", err)
		}

		f.PPPoE.SourceMAC = f.PPPoE.Interface.HardwareAddr
		f.PPPoE.SessionID = uint16(options.PPPoE.SessionID & 0xffff)
		f.PPPoE.VLAN = uint16(options.PPPoE.VLAN & 0xffff)
	}

	return f, nil
}

func (c Creator) NewPacket(dst netip.Addr, seq int) Packet {
	packet := make([]byte, c.packetSize())

	c.SerializePacket(packet, dst, seq)

	return packet
}

func (c Creator) SerializePacket(packet Packet, dst netip.Addr, seq int) int {
	n := 0

	if c.PPPoE.Interface != nil {
		n = c.serializePPPoE(packet)
	} else {
		n = c.serializeEthernet(packet)
	}

	return n + c.serializeIPv6(packet[n:], dst, uint16(seq&0xffff))
}

func (c Creator) packetSize() int {
	if c.PPPoE.Interface != nil {
		return 74
	}

	return 62
}

const (
	etherTypeDot1Q         = 0x8100
	etherTypePPPoESession  = 0x8864
	etherTypeIPv6          = 0x86dd
	pppoeVersion1Session   = 0x1100
	pppoePayloadLength     = 50
	pppIPv6                = 0x0057
	ipv6Headers            = 0x60000000
	ipv6PayloadLength      = 8
	ipv6NextHeaderHopLimit = 0x3a40
	imcpHeader             = 0x8000
	icmpType               = 58
)

func (c Creator) serializePPPoE(dst []byte) int {
	copy(dst[00:06], c.PPPoE.DestMAC)
	copy(dst[06:12], c.PPPoE.SourceMAC)

	binary.BigEndian.PutUint16(dst[12:14], etherTypeDot1Q)
	binary.BigEndian.PutUint16(dst[14:16], c.PPPoE.VLAN)
	binary.BigEndian.PutUint16(dst[16:18], etherTypePPPoESession)
	binary.BigEndian.PutUint16(dst[18:20], pppoeVersion1Session)
	binary.BigEndian.PutUint16(dst[20:22], c.PPPoE.SessionID)
	binary.BigEndian.PutUint16(dst[22:24], pppoePayloadLength)
	binary.BigEndian.PutUint16(dst[24:26], pppIPv6)

	return 26
}

func (c Creator) serializeEthernet(dst []byte) int {
	copy(dst[00:06], c.DestMAC)
	copy(dst[06:12], c.SourceMAC)

	binary.BigEndian.PutUint16(dst[12:14], etherTypeIPv6)

	return 14
}

func (c Creator) serializeIPv6(dst []byte, dstAddr netip.Addr, seq uint16) int {
	srcIP := c.SourceIP.AsSlice()
	dstIP := dstAddr.AsSlice()

	binary.BigEndian.PutUint32(dst[0:4], ipv6Headers)
	binary.BigEndian.PutUint16(dst[4:6], ipv6PayloadLength)
	binary.BigEndian.PutUint16(dst[6:8], ipv6NextHeaderHopLimit)

	copy(dst[8:24], srcIP)
	copy(dst[24:40], dstIP)

	binary.BigEndian.PutUint16(dst[40:42], imcpHeader)
	binary.BigEndian.PutUint16(dst[44:46], c.PID)
	binary.BigEndian.PutUint16(dst[46:48], seq)
	binary.BigEndian.PutUint16(dst[42:44], c.icmpChecksum(dst[40:48], srcIP, dstIP))

	return 48
}

func (c Creator) icmpChecksum(data, srcIP, dstIP []byte) uint16 {
	var csum uint32

	for i := 0; i < net.IPv6len; i += 2 {
		csum += uint32(binary.BigEndian.Uint16(srcIP[i : i+2]))
		csum += uint32(binary.BigEndian.Uint16(dstIP[i : i+2]))
	}

	csum += uint32(icmpType)
	csum += uint32(len(data)) & 0xffff
	csum += uint32(len(data)) >> 16

	return checksum(data, csum)
}

func checksum(data []byte, csum uint32) uint16 {
	for i := 0; i < len(data); i += 2 {
		csum += uint32(binary.BigEndian.Uint16(data[i : i+2]))
	}

	for csum > 0xffff {
		csum = (csum >> 16) + (csum & 0xffff)
	}

	return ^uint16(csum)
}
