package packet_test

import (
	"encoding/hex"
	"net"
	"net/netip"
	"testing"

	"git.slxh.eu/silex/png2ping/pkg/packet"
)

func TestCreator_NewPacket(t *testing.T) {
	creator := packet.Creator{
		SourceIP:   netip.MustParseAddr("2001:db8::1"),
		SourceLink: &net.Interface{HardwareAddr: net.HardwareAddr{0x01, 0x02, 0x03, 0x04, 0x05, 0x06}},
		SourceMAC:  net.HardwareAddr{0x01, 0x02, 0x03, 0x04, 0x05, 0x06},
		DestMAC:    net.HardwareAddr{0x11, 0x12, 0x13, 0x14, 0x15, 0x16},
	}

	addr := netip.MustParseAddr("2001:db8::2")
	got := creator.NewPacket(addr, 27)
	exp := "111213141516" + // Ethernet destination
		"010203040506" + // Ethernet source
		"86dd" + // Ethernet type
		"6000000000083a40" + // IPv6 headers
		"20010db8000000000000000000000001" + // IPv6 source
		"20010db8000000000000000000000002" + // IPv6 destination
		"8000242d0000001b" // ICMP echo with payload

	if exp != hex.EncodeToString(got) {
		t.Errorf("\ngot:  %x\nwant: %s", got, exp)
	}

	t.Logf("Len: %v", len(got))
}

func TestCreator_NewPacket_pppoe(t *testing.T) {
	creator := packet.Creator{
		SourceIP:   netip.MustParseAddr("2001:db8::1"),
		SourceLink: &net.Interface{HardwareAddr: net.HardwareAddr{0x01, 0x02, 0x03, 0x04, 0x05, 0x06}},
		SourceMAC:  net.HardwareAddr{0x01, 0x02, 0x03, 0x04, 0x05, 0x06},
		DestMAC:    net.HardwareAddr{0x11, 0x12, 0x13, 0x14, 0x15, 0x16},
		PPPoE: struct {
			Interface *net.Interface
			SourceMAC net.HardwareAddr
			DestMAC   net.HardwareAddr
			SessionID uint16
			VLAN      uint16
		}{
			Interface: &net.Interface{},
			SourceMAC: net.HardwareAddr{0x21, 0x22, 0x23, 0x24, 0x25, 0x26},
			DestMAC:   net.HardwareAddr{0x31, 0x32, 0x33, 0x34, 0x35, 0x36},
			SessionID: 0x4321,
			VLAN:      18,
		},
	}

	addr := netip.MustParseAddr("2001:db8::2")
	got := creator.NewPacket(addr, 27)
	exp := "313233343536" + "212223242526" + "8100" + // Ethernet source/dest/type
		"00128864" + // VLAN
		"110043210032" + "0057" + // PPPoE session, PPP
		"6000000000083a40" + // IPv6 headers
		"20010db8000000000000000000000001" + // IPv6 source
		"20010db8000000000000000000000002" + // IPv6 destination
		"8000242d0000001b" // ICMP echo with payload

	if exp != hex.EncodeToString(got) {
		t.Errorf("\ngot:  %x\nwant: %s", got, exp)
	}

	t.Logf("Len: %v", len(got))
}

func TestCreator_SerializePacket(t *testing.T) {
	creator := packet.Creator{
		SourceIP:   netip.MustParseAddr("2001:db8::"),
		SourceLink: &net.Interface{HardwareAddr: make(net.HardwareAddr, 6)},
		SourceMAC:  make(net.HardwareAddr, 6),
		DestMAC:    make(net.HardwareAddr, 6),
	}

	addr := netip.MustParseAddr("2001:db8::123")
	pkt := make(packet.Packet, 128)
	allocs := testing.AllocsPerRun(1e6, func() {
		creator.SerializePacket(pkt, addr, 0)
	})

	if allocs > 0 {
		t.Errorf("allocs: %f (>0)", allocs)
	}
}

func BenchmarkCreator_NewPacket(b *testing.B) {
	creator := packet.Creator{
		SourceIP:   netip.MustParseAddr("2001:db8::"),
		SourceLink: &net.Interface{HardwareAddr: make(net.HardwareAddr, 6)},
		SourceMAC:  make(net.HardwareAddr, 6),
		DestMAC:    make(net.HardwareAddr, 6),
	}

	addr := netip.MustParseAddr("2001:db8::123")

	for i := range b.N {
		_ = creator.NewPacket(addr, i)
	}
}

func BenchmarkCreator_SerializePacket(b *testing.B) {
	creator := packet.Creator{
		SourceIP:   netip.MustParseAddr("2001:db8::"),
		SourceLink: &net.Interface{HardwareAddr: make(net.HardwareAddr, 6)},
		SourceMAC:  make(net.HardwareAddr, 6),
		DestMAC:    make(net.HardwareAddr, 6),
	}

	addr := netip.MustParseAddr("2001:db8::123")
	pkt := make(packet.Packet, 128)

	b.ResetTimer()
	b.ReportAllocs()

	for i := range b.N {
		creator.SerializePacket(pkt, addr, i)
	}
}
