package pipe

import (
	"git.slxh.eu/silex/png2ping/pkg/packet"
)

// Pipe represents a pipeline components that connects two message slice channels together.
// Pipes can be used to modify or analyze a message flow.
type Pipe interface {
	// Run the pipe for the given source and destination.
	Run(src <-chan []packet.Packet, dst chan<- []packet.Packet)
}
