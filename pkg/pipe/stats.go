package pipe

import (
	"log"
	"time"

	"git.slxh.eu/silex/png2ping/pkg/packet"
)

// Stats represents a pipe component that prints the message statistics of the given flow.
type Stats struct {
	interval  time.Duration
	count     float64
	sum, prev int
	t         time.Time
}

// NewStats returns a Stats Pipe that prints the statistics on the given interval (in messages).
func NewStats(interval time.Duration) Pipe {
	return &Stats{interval: interval, t: time.Now()}
}

// Run the pipe for the given source and destination.
func (s *Stats) Run(src <-chan []packet.Packet, dst chan<- []packet.Packet) {
	defer close(dst)

	s.t = time.Now()

	for m := range src {
		s.sum += len(m)
		dst <- m

		if time.Since(s.t) > s.interval {
			stop := time.Now()
			diff := stop.Sub(s.t).Seconds()
			s.count += diff

			s.Print(diff)
			s.t = stop
			s.prev = s.sum
		}
	}

	s.Print(time.Since(s.t).Seconds())
}

// Print the packet rate statistics.
func (s *Stats) Print(dt float64) {
	log.Printf("Rate: %10.0f packets/s, average: %10.0f packets/s, total: %10d packets",
		float64(s.sum-s.prev)/dt, float64(s.sum)/s.count, s.sum)
}
