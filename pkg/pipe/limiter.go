package pipe

import (
	"time"

	"git.slxh.eu/silex/png2ping/pkg/packet"
	"git.slxh.eu/silex/png2ping/pkg/pixel"
)

// Limiter is a Pipe component that limits the message rate.
// Note that due to packet batching this rate may be exceeded.
type Limiter struct {
	ticker *time.Ticker
}

// NewLimiter creates a limiter with the given rate as the target limit.
func NewLimiter(rate float64) *Limiter {
	// Calculate packet interval for maximum rate
	interval := time.Duration(1 / rate * 1e9)

	// Create rate limit ticker
	return &Limiter{
		ticker: time.NewTicker(interval * time.Duration(pixel.BufferSize)),
	}
}

// Run the pipe for the given source and destination.
func (l *Limiter) Run(src <-chan []packet.Packet, dst chan<- []packet.Packet) {
	defer close(dst)
	defer l.ticker.Stop()

	for m := range src {
		for i := 0; i < len(m); i += pixel.BufferSize {
			dst <- m[i:min(len(m), i+pixel.BufferSize)]
			<-l.ticker.C
		}
	}
}
