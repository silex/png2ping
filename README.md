# png2ping

A simple tool for converting PNG images to IPv6 christmas tree pings.

## Usage

Download and install the library and tools using:

```sh
go get git.slxh.eu/silex/png2ping
```

To send an image to the tree, use the following:

```sh
png2ping -file pixel.png
```

See `png2ping -help` for more options.

In order to use the binary as a normal user in Linux
`CAP_NET_RAW` capabilities have to be set on the binary, eg:

```sh
sudo setcap cap_net_raw=+ep png2ping
```
