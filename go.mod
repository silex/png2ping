module git.slxh.eu/silex/png2ping

go 1.23

require (
	github.com/asavie/xdp v0.3.4-0.20240826152323-67e5ba17320c
	github.com/vishvananda/netlink v1.3.0
	golang.org/x/net v0.30.0
	golang.org/x/sys v0.26.0
)

require (
	github.com/cilium/ebpf v0.4.0 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/vishvananda/netns v0.0.4 // indirect
)
