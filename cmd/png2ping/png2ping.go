// Copyright 2018 Silke Hofstra
//
// Licensed under the EUPL
//

// png2ping is a tool for transferring PNG images as ICMP echo requests.
package main

import (
	"cmp"
	"errors"
	"flag"
	"log"
	"log/slog"
	"math"
	"net"
	"net/http"
	_ "net/http/pprof"
	"sync"
	"time"

	"golang.org/x/net/context"

	"git.slxh.eu/silex/png2ping/pkg/address"
	"git.slxh.eu/silex/png2ping/pkg/image"
	"git.slxh.eu/silex/png2ping/pkg/packet"
	"git.slxh.eu/silex/png2ping/pkg/pipe"
	"git.slxh.eu/silex/png2ping/pkg/pixel"
	"git.slxh.eu/silex/png2ping/pkg/writer"
)

const (
	addrPrefix    = "2001:610:1908:a000::"
	legacyPrefix  = "2001:4c08:2028:0::"
	statsInterval = 10 * time.Second
)

var (
	ErrUnknownAddressMode = errors.New("unknown address mode")
	ErrNoImageSource      = errors.New("no image source")
	ErrUnknownOutputMode  = errors.New("unknown output mode")
)

func expandPipe(p pipe.Pipe, src, dst chan []packet.Packet) chan []packet.Packet {
	go p.Run(src, dst)
	return dst
}

func initChannels(maxRate float64, showStats bool) (chan<- []packet.Packet, <-chan []packet.Packet) {
	s := make(chan []packet.Packet, 8)
	d := s

	if maxRate > 0 {
		slog.Info("Rate limited", "limit", maxRate)

		d = expandPipe(pipe.NewLimiter(maxRate), d, make(chan []packet.Packet, 8))
	}

	if showStats {
		d = expandPipe(pipe.NewStats(statsInterval), d, make(chan []packet.Packet, 8))
	}

	return s, d
}

func initAddrGenerator(addrMode, dst string, opts address.GeneratorOpts) (address.Generator, error) {
	switch addrMode {
	case "snt":
		return address.NewRGBA(dst, opts)
	case "legacy":
		if dst == addrPrefix {
			dst = legacyPrefix
		}

		return address.NewLegacy(dst, opts), nil
	case "tree":
		return address.NewTree(dst)
	default:
		return nil, ErrUnknownAddressMode
	}
}

func initPixelGenerator(fileName, webURL string, options pixel.GeneratorOptions) (pixel.Generator, error) {
	switch {
	case webURL != "":
		return image.FromWeb(webURL, options)
	case fileName != "":
		return image.FromFile(fileName, options)
	default:
		return nil, ErrNoImageSource
	}
}

func initWriter(outputMode string, link *net.Interface, id int) (writer.Writer, error) {
	switch outputMode {
	case "devnull":
		return writer.NewDevNull(), nil
	case "xdp":
		return writer.NewXDP(link, id)
	case "afpacket":
		return writer.NewAFPacket(link)
	default:
		return nil, ErrUnknownOutputMode
	}
}

func main() {
	var options pixel.GeneratorOptions
	var addrOpts address.GeneratorOpts
	var pktOpts packet.CreatorOpts
	var addrMode, outputMode string
	var fileName, webURL, pprofAddr string
	var pingCount, threads int
	var maxRate float64
	var showStats bool
	var err error

	flag.StringVar(&fileName, "file", "", "Image to convert")
	flag.StringVar(&webURL, "web", "", "Image to convert")

	flag.StringVar(&pprofAddr, "pprof", "", "Enable profiling on the given address")
	flag.IntVar(&pingCount, "count", 0, "Number of times the image is pinged, unlimited by default")
	flag.IntVar(&threads, "threads", 1, "Number of threads/sockets/queues to open for sending pings")
	flag.Float64Var(&maxRate, "max-rate", 0, "Maximum packets/second, unlimited is disabled")
	flag.BoolVar(&showStats, "show-stats", true, "Show transmission statistics")

	flag.StringVar(&addrMode, "addr-mode", "snt", "Address mode: snt, legacy or tree")
	flag.StringVar(&outputMode, "output-mode", "afpacket", "Output mode: afpacket, xdp or devnull")
	flag.StringVar(&addrOpts.ColorMode, "color-mode", "bgra", "Color mode")

	flag.StringVar(&pktOpts.SourceAddr, "addr", "::", "Source address")
	flag.StringVar(&pktOpts.DestAddr, "prefix", addrPrefix, "IPv6 prefix to ping to")
	flag.StringVar(&pktOpts.PPPoE.Interface, "pppoe.interface", "", "Use PPPoE on this interface")
	flag.StringVar(&pktOpts.PPPoE.NextHop, "pppoe.nexthop", "", "MAC address of the next PPPoE hop")
	flag.IntVar(&pktOpts.PPPoE.VLAN, "pppoe.vlan", 0, "VLAN of the PPPoE interface")
	flag.IntVar(&pktOpts.PPPoE.SessionID, "pppoe.session", 0, "PPPoE Session ID")

	flag.IntVar(&addrOpts.Offset.X, "offset-x", 0, "X offset")
	flag.IntVar(&addrOpts.Offset.Y, "offset-y", 0, "Y offset")
	flag.IntVar(&addrOpts.CanvasSize.X, "canvas-size-x", 1920, "X canvas size")
	flag.IntVar(&addrOpts.CanvasSize.Y, "canvas-size-y", 1080, "Y canvas size")
	flag.IntVar(&options.RepeatFrame, "repeat-frame", 1, "Repeat frames for animations")
	flag.BoolVar(&options.Randomize, "randomize", false, "Randomize the pixel order")
	flag.BoolVar(&options.SkipTransparent, "skip-transparent", true, "Don't send transparent pixels")
	flag.DurationVar(&options.ReloadInterval, "reload-interval", 0, "Reload image periodically")

	flag.Parse()
	log.Printf("Prefix is %s (color space %s)", pktOpts.DestAddr, addrOpts.ColorMode)

	if pprofAddr != "" {
		go func() {
			if err := http.ListenAndServe(pprofAddr, nil); err != nil {
				slog.Error("Unable to start pprof webserver", "err", err)
			}
		}()
	}

	options.Generator, err = initAddrGenerator(addrMode, pktOpts.DestAddr, addrOpts)
	if err != nil {
		log.Fatalf("Error creating address generator: %s", err)
	}

	options.Creator, err = packet.NewCreator(pktOpts)
	if err != nil {
		log.Fatalf("Error creating packet pktOpts: %s", err)
	}

	source, err := initPixelGenerator(fileName, webURL, options)
	if err != nil {
		log.Fatalf("Error creating packet pktOpts: %s", err)
	}

	s, d := initChannels(maxRate, showStats)

	var wg sync.WaitGroup
	for i := range threads {
		wg.Add(1)

		w, err := initWriter(outputMode, cmp.Or(options.Creator.PPPoE.Interface, options.SourceLink), i)
		if err != nil {
			log.Fatalf("Error creating writer: %s", err)
		}

		go func() {
			w.Run(d)
			wg.Done()
		}()
	}

	if pingCount > 0 {
		log.Printf("Sending image %v times", pingCount)
	}

	source.Generate(context.Background(), cmp.Or(pingCount, math.MaxInt), s)
	close(s)
	wg.Wait()
}
